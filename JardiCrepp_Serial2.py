# JardiCREPP - www.crepp.org
# florian.mausy@gmail.com - yvoz.lg@gmail.com - janvier 2020
# extrait de SimpleGCodeGUI
# par X. HINAULT - Déc 2012 - Avril 2015 - www.mon-club-elec.fr
# GPLv3

import os,sys
import serial 
import time
import datetime

#print(sys.version)

class RunGCode(object):
    def __init__(self):
        self.ser = None
    
    def open(self):
        if self.ser != None:
            if self.ser.is_open:
                self.ser.close()
            
        try: # essaie d'exécuter les instructions 
            self.ser = serial.Serial('/dev/ttyACM0', baudrate=115200, bytesize=8, parity='N', stopbits=1, timeout=0)
            # '/dev/ttyUSB0' sur certain système
            return True
        except: # si erreur initialisation 
            print("Erreur initialisation Série")
            return False
    
    def close(self):
        if self.ser:
            if self.ser.is_open:
                self.ser.close()
    
    def sendGCode(self, gcodeIn):
        if self.ser == None:
            return
        if self.ser.is_open == False:
            return
        
        print(gcodeIn)
        gcodeLines = gcodeIn.splitlines()
        
        # défile les lignes du gcode
        for line in gcodeLines: # défile les lignes
            line = line.strip()
            if line == "": continue
            if line[:1] == ";": continue
            #
            line += "\n"
            self.ser.write(line.encode('utf-8')) 
            print("---->", line) 
            
            #
            time.sleep(0.01) 
            flagOk = False
            
            while flagOk == False: # on attend réception <ok>
                while self.ser.in_waiting > 0: 
                    #out = ser.read() 
                    out = self.ser.readline()
                    if out != "": 
                        out = out.strip()
                        print("<----", out)
                        if out == "ok".encode('utf-8'): # Smoothie : "ok" - GRBL : "<ok>"
                            print("<---ok")
                            flagOk = True
                            break
                    else:
                        print("---")
                time.sleep(0.01) 
            # fin du flagOk
        # fin du for line

    def loadFile(self, pathfile):
        try: # essaie d'exécuter les instructions
            # myFile = open(pathfile, "r", encoding="UTF-8") # ouvre le fichier en lecture UTF-8 par defaut
            myFile = open(pathfile, "r", encoding="ISO-8859-1")
            myFileContent = myFile.read() # lit le contenu du fichier
            # print(myFileContent)
            myFile.close() # ferme le fichier - tant que le fichier reste ouvert, il est inacessible à d'autres ressources
            return myFileContent
        except: # si erreur initialisation 
            print("Erreur Ouverture de fichier")
            return False
    
    def getFileName(self):
        date = datetime.datetime.now()
        filename = "JardiCrepp_"+ str(date)[0:10] +".gcode" 
        print(filename)
        return filename



# -------------------------------------------------
# fonction principale de lancement de l'application 
def main(args):
    rgc = RunGCode() # initialise l'instance
    filename = rgc.getFileName() # nom du fichier GCode
    gcode = rgc.loadFile(filename) # lit et récupère le contenu
    if rgc.open(): # ouvre la liaison série
        rgc.sendGCode(gcode) # envoie le GCode
        rgc.close() # ferme la lisaison série

# -- pour rendre le code exécutable
if __name__ == "__main__" : # pour rendre le code exécutable 
    main(sys.argv) # appelle la fonction main

